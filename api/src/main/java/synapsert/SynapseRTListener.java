package synapsert;

import io.qameta.allure.TmsLink;
import lombok.extern.slf4j.Slf4j;
import org.testng.*;
import org.testng.annotations.Test;
import synapsert.enums.SynapseRTStatus;
import synapsert.enums.TestPlanKeysEnum;
import synapsert.models.TestCycleRequest;
import synapsert.models.TestCycleResponse;
import synapsert.models.UpdateTestRunRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static synapsert.SynapseRTRequests.getSynapseRTInstance;
import static synapsert.enums.SynapseRTStatus.*;
import static synapsert.enums.TestCycleActionEnum.COMPLETE;
import static synapsert.enums.TestCycleActionEnum.START;

@Slf4j
public class SynapseRTListener implements ITestListener, ISuiteListener {

    private static TestPlanKeysEnum testPlanKey;

    private static TestPlanKeysEnum getSuiteName() {
        switch (System.getProperty("suite")) {
            case "smoke":
                testPlanKey = TestPlanKeysEnum.SMOKE;
                break;
            case "regression":
                testPlanKey = TestPlanKeysEnum.REGRESSION;
                break;
            default:
                testPlanKey = TestPlanKeysEnum.SMOKE;
                break;
        }
        return testPlanKey;
    }

    private String cycleName = getSuiteName().getName() + " " +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));

    private TestCycleRequest testCycleRequest = TestCycleRequest.builder()
            .name(cycleName)
            //.environment(System.getProperty("browser.name") + " " + System.getProperty("capabilities.version"))
            .plannedStartDate(LocalDate.now().toString())
            //.plannedEndDate(LocalDate.now().toString())
            .build();

    @Override
    public void onStart(ISuite suite) {
        List<TestCycleResponse> testCycleResponseList = getSynapseRTInstance().getTestCycles(testPlanKey);
        if (testCycleResponseList.stream().noneMatch(cycle -> cycle.getName()
                .equalsIgnoreCase(testCycleRequest.getName()))) {
            getSynapseRTInstance().createTestCycle(testPlanKey, testCycleRequest)
                    .updateTestCycle(testPlanKey, testCycleRequest.getName(), START);
        }
    }

    @Override
    public void onFinish(ISuite suite) {
        TestCycleResponse response = getTestCycleByName(testCycleRequest.getName());
        if (!isTestRunWithStatusPresent(response, FAILED)) {
            getSynapseRTInstance().updateTestCycle(testPlanKey, testCycleRequest.getName(), COMPLETE);
        }
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        setStatus(iTestResult, PASSED);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        setStatus(iTestResult, FAILED);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        setStatus(iTestResult, NOT_TESTED);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
    }

    private void setStatus(ITestResult iTestResult, SynapseRTStatus synapseRTStatus) {
        if (getTestCaseId(iTestResult) == null) {
            return;
        }
        UpdateTestRunRequest updateTestRunRequest = UpdateTestRunRequest.builder()
                .result(synapseRTStatus.getStatus())
                .testcaseKey(getTestCaseId(iTestResult))
                .build();
        getSynapseRTInstance().updateTestRun(testPlanKey, updateTestRunRequest, testCycleRequest.getName());
    }

    private String getTestCaseId(ITestResult iTestResult) {
        Test test = iTestResult.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Test.class);
        TmsLink tmsLink = iTestResult.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TmsLink.class);
        Object[] params = iTestResult.getParameters();
        String testCaseID = null;

        if (tmsLink == null) {
            log.warn("Empty testCase ID");
        } else if (params.length > 0 && !test.dataProvider().isEmpty()) {
            testCaseID = String.valueOf(params[0]);
        } else if (!tmsLink.value().isEmpty()) {
            testCaseID = tmsLink.value();
        }
        return testCaseID;
    }

    private TestCycleResponse getTestCycleByName(String testCycleName) {
        return getSynapseRTInstance().getTestCycles(testPlanKey).stream().filter(cycle -> cycle.getName()
                .equalsIgnoreCase(testCycleName)).findAny().get();
    }

    private boolean isTestRunWithStatusPresent(TestCycleResponse testCycleResponse, SynapseRTStatus synapseRTStatus) {
        return getSynapseRTInstance().getTestRuns(testPlanKey, testCycleResponse.getId()).stream()
                .anyMatch(run -> run.getStatus().equalsIgnoreCase(synapseRTStatus.getStatus()));
    }
}
