package synapsert.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TestCycleActionEnum {

    START("Start"),
    COMPLETE("Complete"),
    ABORT("Abort"),
    RESUME("Resume");

    private String action;
}
