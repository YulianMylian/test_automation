package synapsert.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TestPlanKeysEnum {

    SMOKE("Smoke", "ELEM-3117"),
    REGRESSION("Regression test plan", "ELEM-3116");

    private String name;
    private String key;
}
