package synapsert.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SynapseRTStatus {

    PASSED("Passed"), NOT_TESTED("Not Tested"), FAILED("Failed"), BLOCKED("Blocked");

    final String status;
}
