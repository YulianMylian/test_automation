package synapsert;

import domain.models.Configuration;
import engine.api.RestClient;
import engine.api.authenticationstrategy.BasicAuthentication;
import io.restassured.response.Response;
import synapsert.enums.TestCycleActionEnum;
import synapsert.enums.TestPlanKeysEnum;
import synapsert.models.TestCycleRequest;
import synapsert.models.TestCycleResponse;
import synapsert.models.TestRunsResponse;
import synapsert.models.UpdateTestRunRequest;

import java.util.List;
import java.util.Objects;

import static domain.models.Credentials.getSynapseRtCredential;

public class SynapseRTRequests extends RestClient {
    private static final String ADD_CYCLE_TEST_PATH = "/rest/synapse/latest/public/testPlan/%s/addCycle";
    private static final String UPDATE_TEST_CYCLE_PATH = "/rest/synapse/latest/public/testPlan/%s/cycle/%s/wf/%s";
    private static final String GET_TEST_RUNS_PATH = "/rest/synapse/latest/public/testPlan/%s/cycle/%d/testRunsByCycleId";
    private static final String GET_TEST_CYCLES_PATH = "/rest/synapse/latest/public/testPlan/%s/cycles";
    private static final String UPDATE_TEST_RUN_PATH = "/rest/synapse/latest/public/testPlan/%s/cycle/%s/updateTestRun";
    private static volatile SynapseRTRequests synapseRTInstance;

    static SynapseRTRequests getSynapseRTInstance() {
        SynapseRTRequests localInstance = synapseRTInstance;
        if (Objects.isNull(localInstance)) {
            synchronized (SynapseRTRequests.class) {
                localInstance = synapseRTInstance;
                if (Objects.isNull(localInstance)) {
                    synapseRTInstance = localInstance = new SynapseRTRequests();
                }
            }
        }
        return localInstance;
    }

    @Override
    protected Configuration defaultConfiguration() {
        return new Configuration(getSynapseRtBaseApiUrl(), new BasicAuthentication(getSynapseRtCredential()));
    }

    SynapseRTRequests createTestCycle(TestPlanKeysEnum testPlanKeysEnum, TestCycleRequest testCycleRequest) {
        post(String.format(ADD_CYCLE_TEST_PATH, testPlanKeysEnum.getKey()), testCycleRequest, Response.class)
                .expectingStatusCode(200);
        return this;
    }

    SynapseRTRequests updateTestCycle(TestPlanKeysEnum testPlanKeysEnum, String cycleName, TestCycleActionEnum testCycleActionEnum) {
        put(String.format(UPDATE_TEST_CYCLE_PATH, testPlanKeysEnum.getKey(),
                cycleName, testCycleActionEnum.getAction()), Response.class).expectingStatusCode(200);
        return this;
    }

    List<TestRunsResponse> getTestRuns(TestPlanKeysEnum testPlanKeysEnum, int cycleId) {
        return get(String.format(GET_TEST_RUNS_PATH, testPlanKeysEnum.getKey(), cycleId), TestRunsResponse.class)
                .expectingStatusCode(200).readEntities("$");
    }

    List<TestCycleResponse> getTestCycles(TestPlanKeysEnum testPlanKeysEnum) {
        return get(String.format(GET_TEST_CYCLES_PATH, testPlanKeysEnum.getKey()),
                TestCycleResponse.class).expectingStatusCode(200).readEntities("$");
    }

    SynapseRTRequests updateTestRun(TestPlanKeysEnum testPlanKeysEnum, UpdateTestRunRequest updateTestRunRequest,
                                    String cycleName) {
        post(String.format(UPDATE_TEST_RUN_PATH, testPlanKeysEnum.getKey(), cycleName), updateTestRunRequest,
                Response.class).expectingStatusCode(200);
        return this;
    }
}