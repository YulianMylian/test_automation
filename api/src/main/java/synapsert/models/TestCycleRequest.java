package synapsert.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestCycleRequest {
    private String environment;
    private String plannedStartDate;
    private String name;
    private String plannedEndDate;
}