package synapsert.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestCycleResponse{
	private String cycleStartedDate;
	private boolean deleteable;
	private boolean aborted;
	private String cycleCompletedDate;
	private boolean draft;
	private String name;
	private String nameHtml;
	private boolean active;
	private boolean completed;
	private int id;
	private boolean adhocTestCycle;
	private String status;
}