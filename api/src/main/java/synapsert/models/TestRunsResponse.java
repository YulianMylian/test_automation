package synapsert.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestRunsResponse {
    private String executedBy;
    private String executionTimeStamp;
    private String summary;
    private String testCaseKey;
    private String comment;
    private int id;
    private String status;
    private String testerName;
}