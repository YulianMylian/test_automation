package heroku.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppModel {
    private Boolean acm;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
    private String id;
    @JsonProperty("git_url")
    private String gitUrl;
    private Boolean maintenance;
    private String name;
    @JsonProperty("released_at")
    private LocalDateTime releasedAt;
    @JsonProperty("updated_at")
    private LocalDateTime updatedAt;
    @JsonProperty("web_url")
    private String webUrl;
}