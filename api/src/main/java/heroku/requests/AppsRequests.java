package heroku.requests;

import domain.models.Configuration;
import domain.models.Credentials;
import engine.api.RestClient;
import engine.api.authenticationstrategy.TokenAuthentication;
import heroku.models.AppModel;
import io.qameta.allure.Step;

import java.util.List;

import static domain.models.Credentials.getHerokuCredential;
import static engine.api.Services.HEROKU;

public class AppsRequests extends RestClient {
    private static final String APPS_PATH = "/apps";

    @Override
    protected Configuration defaultConfiguration() {
        Credentials herokuCredential = getHerokuCredential();
        return new Configuration(getHerokuBaseApiUrl(), new TokenAuthentication(herokuCredential, HEROKU));
    }

    @Step("Get Heroku apps data through API")
    public List<AppModel> getApps(int expectingStatusCode) {
        return get(APPS_PATH, AppModel.class).expectingStatusCode(expectingStatusCode).readEntities();
    }
}