package salesforce.requests;

import domain.models.Configuration;
import domain.models.Credentials;
import engine.api.*;
import engine.api.authenticationstrategy.TokenAuthentication;
import io.qameta.allure.Step;
import salesforce.models.describesobject.DescribeSObjectModel;

import static domain.models.Credentials.getSalesForceCredential;
import static engine.api.Services.SALESFORCE;

public class DescribeSObjectRequests extends RestClient {

    public static final String DESCRIBE_S_OBJECT_PATH = "services/data/v45.0/sobjects/Account/describe";

    @Override
    protected Configuration defaultConfiguration() {
        Credentials salesForceCredential = getSalesForceCredential();
        return new Configuration(getSalesForceBaseApiUrl(salesForceCredential),
                new TokenAuthentication(salesForceCredential, SALESFORCE));
    }

    @Step("Get SalesForce describeSObject through API")
    public DescribeSObjectModel getDescribeSObject(int expectingStatusCode) {
        return get(DESCRIBE_S_OBJECT_PATH, DescribeSObjectModel.class).expectingStatusCode(expectingStatusCode)
                .readEntity();
    }
}
