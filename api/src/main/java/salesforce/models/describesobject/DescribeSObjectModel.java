package salesforce.models.describesobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DescribeSObjectModel {
    private Boolean activateable;
    private Boolean compactLayoutable;
    private Boolean createable;
    private Boolean custom;
    private Boolean customSetting;
    private Boolean deletable;
    private Boolean deprecatedAndHidden;
    private Boolean feedEnabled;
    private Boolean hasSubtypes;
    private Boolean isSubtype;
    private String keyPrefix;
    private String label;
    private String labelPlural;
    private Boolean layoutable;
    private Boolean mergeable;
    private Boolean mruEnabled;
    private String name;
    private Boolean networkScopeFieldName;
    private Boolean queryable;
    private Boolean replicateable;
    private Boolean retrieveable;
    private Boolean searchLayoutable;
    private Boolean searchable;
    private Boolean triggerable;
    private Boolean undeletable;
    private Boolean updateable;
    @JsonProperty("urls")
    private UrlsModel urls;
}