package salesforce.models.describesobject;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UrlsModel {
    @JsonProperty("compactLayouts")
    private String compactLayouts;
    @JsonProperty("rowTemplate")
    private String rowTemplate;
    @JsonProperty("approvalLayouts")
    private String approvalLayouts;
    @JsonProperty("uiDetailTemplate")
    private String uiDetailTemplate;
    @JsonProperty("uiEditTemplate")
    private String uiEditTemplate;
    @JsonProperty("defaultValues")
    private String defaultValues;
    @JsonProperty("listviews")
    private String listviews;
    @JsonProperty("describe")
    private String describe;
    @JsonProperty("uiNewRecord")
    private String uiNewRecord;
    @JsonProperty("quickActions")
    private String quickActions;
    @JsonProperty("layouts")
    private String layouts;
    @JsonProperty("sobject")
    private String sobject;
}