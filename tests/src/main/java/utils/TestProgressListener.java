package utils;

import lombok.extern.java.Log;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Log
public class TestProgressListener implements ITestListener {

    private static int cachedTotalNumberOfTests;
    private static Set<String> cachedAlreadyExecutedTests = Collections.newSetFromMap(new ConcurrentHashMap<>());

    private int getTotalNumberOfTests(ITestContext context) {
        if (cachedTotalNumberOfTests == 0) {
            cachedTotalNumberOfTests = context.getSuite().getAllMethods().size();
        }

        return cachedTotalNumberOfTests;
    }

    private String getTestsProgressPercentage(ITestContext context) {
        return (100 * cachedAlreadyExecutedTests.size()) / getTotalNumberOfTests(context) + "%";
    }

    private String getTestName(ITestResult result) {
        Object[] testParameters = result.getParameters();
        if (testParameters.length == 0) {
            return result.getName();
        }

        return result.getName() + Arrays.toString(testParameters);
    }

    private String buildTestStatusLog(String message, ITestResult result) {
        return String.format("Thread %1$-3s Finished %2$-4s %3$-8s %4$s",
                Thread.currentThread().getId(),
                getTestsProgressPercentage(result.getTestContext()),
                message,
                getTestName(result));
    }

    @Override
    public void onTestStart(ITestResult result) {
        log.info(buildTestStatusLog("Starting", result));
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        cachedAlreadyExecutedTests.add(result.getName());
        log.info(buildTestStatusLog("Passed", result));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        cachedAlreadyExecutedTests.add(result.getName());
        log.warning(buildTestStatusLog("Failed", result));
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        cachedAlreadyExecutedTests.add(result.getName());
        log.warning(buildTestStatusLog("Skipped", result));
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }
}