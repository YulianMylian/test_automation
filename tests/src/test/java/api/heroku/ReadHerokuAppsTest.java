package api.heroku;

import api.BaseApiTest;
import heroku.models.AppModel;
import heroku.requests.AppsRequests;
import io.qameta.allure.*;
import org.testng.annotations.Test;

import javax.inject.Inject;
import java.util.List;

import static api.JiraConstants.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@Epic(HEROKU_INTEGRATION_EPIC)
@Feature(HEROKU_INTEGRATION_FEATURE)
@Story(HEROKU_INTEGRATION_STORY)
public class ReadHerokuAppsTest extends BaseApiTest {

    private AppsRequests appsRequests = new AppsRequests();

    @TmsLink("ELEM-3118")
    @Issue("possible_issue_name")
    @Test(description = "Verify that user is able to read Heroku apps data")
    public void testReadSynapseRtTestCases() {
        List<AppModel> herokuApps = appsRequests.getApps(200);

        assertThat(herokuApps)
                .as("Heroku apps entities data should be found")
                .isNotEmpty();
    }
}