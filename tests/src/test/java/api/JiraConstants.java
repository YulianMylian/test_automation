package api;

public interface JiraConstants {
    //Stories by epics
    String SYNAPSE_RT_INTEGRATION_EPIC = "SynapseRt Test Epic";
    String SYNAPSE_RT_INTEGRATION_STORY = "SynapseRt Test Story";

    String SALESFORCE_INTEGRATION_EPIC = "Salesforce Test Epic";
    String SALESFORCE_INTEGRATION_STORY = "Salesforce Test Story";

    String HEROKU_INTEGRATION_EPIC = "Heroku Test Epic";
    String HEROKU_INTEGRATION_STORY = "Heroku Test Story";

    //Features
    String SYNAPSE_RT_INTEGRATION_FEATURE = "SynapseRt Test Feature";
    String SALESFORCE_INTEGRATION_FEATURE = "Salesforce Test Feature";
    String HEROKU_INTEGRATION_FEATURE = "Heroku Test Feature";
}