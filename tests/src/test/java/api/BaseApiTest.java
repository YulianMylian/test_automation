package api;

import org.testng.annotations.Listeners;
import synapsert.SynapseRTListener;
import utils.TestProgressListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

@Listeners({TestProgressListener.class})
public class BaseApiTest {
    static {
        InputStream stream = BaseApiTest.class.getClassLoader().
                getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}