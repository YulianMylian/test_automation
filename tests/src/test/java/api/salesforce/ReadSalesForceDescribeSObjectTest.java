package api.salesforce;

import api.BaseApiTest;
import domain.models.Credentials;
import io.qameta.allure.*;
import org.testng.annotations.Test;
import salesforce.models.describesobject.DescribeSObjectModel;
import salesforce.requests.DescribeSObjectRequests;

import javax.inject.Inject;

import static api.JiraConstants.*;
import static engine.api.RestClient.getSalesForceBaseApiUrl;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static salesforce.requests.DescribeSObjectRequests.DESCRIBE_S_OBJECT_PATH;

@Epic(SALESFORCE_INTEGRATION_EPIC)
@Feature(SALESFORCE_INTEGRATION_FEATURE)
@Story(SALESFORCE_INTEGRATION_STORY)
public class ReadSalesForceDescribeSObjectTest extends BaseApiTest {

    private DescribeSObjectRequests describeSObjectRequests = new DescribeSObjectRequests();

    @TmsLink("ELEM-3119")
    @Issue("possible_issue_name")
    @Test(description = "Verify that user is able to read salesforce describeSObject")
    public void testReadSalesForceDescribeSObject() {
        DescribeSObjectModel describeSObjectModel = describeSObjectRequests.getDescribeSObject(200);

        assertThat(describeSObjectModel)
                .as("Salesforce describeSObject should be found")
                .isNotNull();

        assertThat(describeSObjectModel.getUrls().getDescribe())
                .as("Salesforce describeSObject Urls object should equal to salesforce instance url")
                .isEqualTo("/" + DESCRIBE_S_OBJECT_PATH);

        assertThat(describeSObjectModel.getUrls().getUiDetailTemplate())
                .as("Salesforce describeSObject Urls object should contain its own API path URL")
                .contains(getSalesForceBaseApiUrl(Credentials.getSalesForceCredential()));
    }
}