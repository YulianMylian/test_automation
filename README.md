# Technetium Test Automation Project

This is Technetium test automation project. It supports API testing.

## Table of Contents

1. Environment requirements
2. Configuring test project
3. Running tests
4. Allure report
5. Project structure
6. Branching strategy

## 1. Environment requirements

**Required:** Java Development Kit 11 (JDK11) and maven 3 should be present on environment in order to develop and/or run the tests.

Also, https://projectlombok.org/ is used to simplify creating/editing Java POJOs (data models). So in order to compile Project from IDE Lombok plugin should be installed.

**Optionally:**

## 2. Configuring test project

Run this command from the start to ensure that all project dependencies installed

```
$ mvn clean install -U -DskipTests=true
```

## 3. Running tests

In most simple way this command could be used to start tests execution:

```
$ mvn clean test
```

`mvn clean` cleans the `target` folders before test run. It's very important to include this command, as Allure report tool may include some results from previous runs into the report.

`mvn test` is a command for actual test execution. It should usually contain additional parameters, like `driver.type`, `web.url` etc.
 
Such properties could be set alongside `mvn test` command: 
 
Run tests with enabled SynapseRT
 
 ```
$ mvn clean test -Dsynapsert.listener=synapsert.SynapseRTListener
 ```

Additional properties:

`-Dthreads=N` - amount of threads for multithreaded tests execution. Default value is `5`.

`-Dsuite=name` - suite name from `test/resources/suites/` directory. Default value is `suite` which should contain all available tests.

Environmental properties (could be used in case different environments will be provided for testing):

`-DsynapseRt.api.base.url=https://jira-pilot.realtechdx.com`

`-Duser.username=username` - credentials's username to be used in tests.
  
`-Duser.password=password` - credentials's password to be used in tests.
Please note that properties could be supplied with `-D` option alongside `mvn` command or set in `resources\config.properties` file. In case properties are specified with both `-D` option and `resources\config.properties` file, property specified with `-D` option overrides property from `resources\config.properties` file. 

Properties `threads` and `suite` are specified in `tests/pom.xml` and could be overridden only with `-D` option alongside `mvn` command.
 
## 4. Allure report

Once tests were executed `tests` module will contain `target\allure-results` directory. Content of that directory is used for Allure report generating.

In order to generate report after run use `mvn allure:report`. Once command is completed `allure-report` directory is created in project root. You can observe the report by opening `allure-report\index.html` in web browser.

Also, `mvn allure:serve` could be used for both report generating and opening it in your default web browser. Please note that in this case report is hosted through started jetty server.

## 5. Project structure

* `api` module should contain methods for creating/reading test entities through API
* `common` module should contain general Java code reused across other modules
* `tests` module should contain automated test cases

## 6. Branching strategy

In order to support deployment environments next branches are created:
* `master` branch should only contain test automation code that could be executed against released AUT (Application Under Test), typically code to it should be merged after release (once in 2-3 months).
* `stage` branch should only contain test automation code that can be executed against AUT (Application Under Test) deployed to **UAT** environments, typically code to it should be merged after sprint end (once in 2-3 weeks).
* `develop` branch should contain test automation code that is currently in development.

Once work on new automated test case or additional test framework functionality started new branch from `develop` should be created. When the work is done it is merged into `develop` through pull request.

In order to support clean version control history commits with short names like "Fix" etc must be avoided.
Commit name should contain succinct description of the change and have next format:

* use the imperative, present tense: "change" not "changed" nor "changes"

* capitalize first letter

* no dot (.) at the end