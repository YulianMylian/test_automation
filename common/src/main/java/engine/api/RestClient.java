package engine.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import domain.models.Configuration;
import domain.models.Credentials;
import engine.utils.SalesforceLoginUtil;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.java.Log;
import org.hamcrest.Matchers;
import utils.PropertiesReader;

import java.util.Map;

import static engine.utils.ReportUtil.logResponseTime;
import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.ObjectMapperConfig.objectMapperConfig;
import static io.restassured.http.ContentType.JSON;

@Log
public abstract class RestClient {

    public static int DEFAULT_GET_LIMIT = 5;
    public static int MAX_GET_LIMIT = 1000;
    public static int DEFAULT_OFFSET = 0;

    static {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
        objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        config = config().objectMapperConfig(objectMapperConfig().jackson2ObjectMapperFactory((cls, charset) -> objectMapper));
        RestAssured.filters(new AllureRestAssured());
    }

    /**
     * Set up default configuration for API requests.
     *
     * @return - {@link Configuration}
     */
    protected abstract Configuration defaultConfiguration();

    /**
     * Retrieve authenticated request specification to submit the following API queries.
     *
     * @return - {@link RequestSpecification}
     */
    private RequestSpecification getRequestSpecification() {
        return defaultConfiguration().getAuthenticationStrategy().authenticate();
    }

    /**
     * API response logging.
     *
     * @param response - {@link Response} response.
     */
    private void logResponse(Response response) {
        response.then().log().status();
        response.then().log().ifStatusCodeMatches(Matchers.greaterThan(300));
    }

    /**
     * Perform a GET request to a path.
     *
     * @param path - {@link String} the path to send the request.
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <F> ResponseWrapper<F> get(String path, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl()).get(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("GET %s%s", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a GET request to a path with parameter names and their values.
     *
     * @param path - {@link String} the path to send the request.
     * @param responseClass - response entity class.
     * @param queryParams - {@link Map<String, String>} the parameter names and their values
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <F> ResponseWrapper<F> get(String path, Map<String, String> queryParams, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl())
                .queryParams(queryParams).get(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("GET %s%s with filters %s", defaultConfiguration().getBaseUrl(),path, queryParams),
                response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a POST request to a path.
     *
     * @param path - {@link String} the path to send the request.
     * @param payload - the object to serialize and send with the request
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <T, F> ResponseWrapper<F> post(String path, T payload, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl())
                .body(payload).post(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("POST %s%s", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a PATCH request to a path.
     *
     * @param path - {@link String} the path to send the request.
     * @param payload - the object to serialize and send with the request
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <T, F> ResponseWrapper<F> patch(String path, T payload, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl())
                .body(payload).patch(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("PATCH %s%s", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a PUT request to a path with payload.
     *
     * @param path - {@link String} the path to send the request.
     * @param payload - the object to serialize and send with the request
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <T, F> ResponseWrapper<F> put(String path, T payload, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl())
                .body(payload).put(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("PUT %s%s with payload", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a PUT request to a path without payload.
     *
     * @param path - {@link String} the path to send the request.
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <F> ResponseWrapper<F> put(String path, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl()).put(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("PUT %s%s without payload", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Perform a DELETE request to a path.
     *
     * @param path - {@link String} the path to send the request.
     * @param responseClass - response entity class.
     * @return - {@link ResponseWrapper<F>} the response of the request.
     */
    public <F> ResponseWrapper<F> delete(String path, Class<F> responseClass) {
        Response response = getRequestSpecification().baseUri(defaultConfiguration().getBaseUrl()).delete(path);
        logResponse(response);
        logResponseTime(response);
        checkResponse(String.format("DELETE %s%s", defaultConfiguration().getBaseUrl(), path), response);
        return new ResponseWrapper<>(response, responseClass);
    }

    /**
     * Check API response.
     *
     * @param requestDescription - {@link String} request description.
     * @param response - {@link Response} response entity class.
     */
    public void checkResponse(String requestDescription, Response response) {
        if (response.getStatusLine().contains("Unauthorized")) {
            throw new RuntimeException("Unauthorized in API response");
        }

        int statusCode = response.getStatusCode();
        if (statusCode >= 500) {
            String errorMessage = new StringBuilder()
                    .append("Request: ").append(requestDescription).append("\n")
                    .append("Response - server error: ").append(statusCode).append("\n")
                    .append("With body: ").append(response.getBody().asString())
                    .toString();
            throw new RuntimeException(errorMessage);
        }
        if (statusCode >= 400 && statusCode < 500) {
            String errorMessage = new StringBuilder()
                    .append("Request: ").append(requestDescription).append("\n")
                    .append("Response - client error: ").append(statusCode).append("\n")
                    .append("With body: ").append(response.getBody().asString())
                    .toString();
            throw new RuntimeException(errorMessage);
        }
    }

    /**
     * Read synapseRt base API url.
     *
     * @return - {@link String} synapseRt base API url.
     */
    public static String getSynapseRtBaseApiUrl() {
        return PropertiesReader.getProperty("synapseRt.base.url");
    }

    /**
     * Read SalesForce base API url.
     *
     * @return - {@link String} salesforce base API url.
     */
    public static String getSalesForceBaseApiUrl(Credentials credentials) {
        return SalesforceLoginUtil.getSession(credentials).getInstanceUrl();
    }

    //TODO: get heroku base url
    /**
     * Read Heroku base API url.
     *
     * @return - {@link String} heroku base API url.
     */
    public static String getHerokuBaseApiUrl() {
        return PropertiesReader.getProperty("heroku.base.url");
    }
}