package engine.api;

import lombok.Getter;

/**
 * Integrated services.
 */
@Getter
public enum Services {
    SALESFORCE("salesforce"),
    SYNAPSE_RT("synapseRt"),
    HEROKU("heroku");

    String serviceName;

    Services(String serviceName) {
        this.serviceName = serviceName;
    }
}