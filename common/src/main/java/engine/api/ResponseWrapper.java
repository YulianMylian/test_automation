package engine.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import lombok.AllArgsConstructor;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@AllArgsConstructor
public class ResponseWrapper<T> {
    private final Response response;
    private final Class<T> responseClass;

    public Response getResponse() {
        return response;
    }

    public T readEntity() {
        return response.getBody().as(responseClass);
    }

    public String readEntity(String jsonPath) {
        return response.getBody().jsonPath().getString(jsonPath);
    }

    public List<T> readEntities() {
        return response.getBody().jsonPath().getList("", responseClass);
    }

    public List<T> readEntities(String path) {
        return response.getBody().jsonPath().getList(path, responseClass);
    }

    public ResponseWrapper<T> expectingStatusCode(int statusCode) {
        assertThat("Response code differs", response.getStatusCode(), is(statusCode));
        return this;
    }

    public ResponseWrapper<T> expectingContentType(String contentType) {
        assertThat("Content type differs", response.getContentType(), is(contentType));
        return this;
    }

    public T readEntityAsMap(String path) {
        return new ObjectMapper().convertValue(response.getBody().jsonPath().getMap(path), responseClass);
    }
}