package engine.api.authenticationstrategy;

import domain.models.Credentials;
import engine.api.Services;
import engine.utils.TokenProvider;
import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;


import static engine.utils.HerokuLoginUtil.HEROKU_HEADER_MEDIA_TYPE;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class TokenAuthentication implements AuthenticationStrategy {

    private Services service;
    private Credentials credentials;

    public TokenAuthentication(Credentials credentials, Services service) {
        this.service = service;
        this.credentials = credentials;
    }

    @Override
    public RequestSpecification authenticate() {
        Header authorizationHeader = TokenProvider.getAuthorizationHeader(credentials, service);
        if (service.getServiceName().equals(Services.HEROKU.getServiceName())) {
            return given().contentType(JSON)
                    .header(authorizationHeader).accept(HEROKU_HEADER_MEDIA_TYPE);
        } else {
            return given().contentType(JSON)
                    .header(authorizationHeader);
        }
    }
}