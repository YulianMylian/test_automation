package engine.api.authenticationstrategy;

import domain.SensibleText;
import domain.models.Credentials;
import io.restassured.specification.RequestSpecification;

import static io.qameta.allure.Allure.addAttachment;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class BasicAuthentication implements AuthenticationStrategy {

    private Credentials credentials;

    public BasicAuthentication(Credentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public RequestSpecification authenticate() {
        addAttachment("currentUser", new Object() {
            String username = credentials.getUsername();
            SensibleText password = new SensibleText(credentials.getPassword());
            @Override
            public String toString() {
                return new StringBuilder()
                        .append("username:\t").append(username).append("\n")
                        .append("password:\t").append(password).toString();
            }
        }.toString());
        return given().contentType(JSON).auth().preemptive()
                .basic(credentials.getUsername(), credentials.getPassword());
    }
}