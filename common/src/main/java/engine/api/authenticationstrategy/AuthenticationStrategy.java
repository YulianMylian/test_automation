package engine.api.authenticationstrategy;

import io.restassured.specification.RequestSpecification;

public interface AuthenticationStrategy {

    RequestSpecification authenticate();

}