package engine.api;

import lombok.Getter;

/**
 * Query parameter names that'll be sent with the request.
 */
@Getter
public enum QueryParameters {
    LIMIT("Limit"),
    OFFSET("Offset"),
    GRANT_TYPE("grant_type"),
    CLIENT_ID("client_id"),
    CLIENT_SECRET("client_secret"),
    REDIRECT_URL("redirect_url"),
    USERNAME("username"),
    PASSWORD("password");

    private final String queryParameter;

    QueryParameters(String queryParameter) {
        this.queryParameter = queryParameter;
    }

    public String getQueryParameter() {
        return queryParameter;
    }
}