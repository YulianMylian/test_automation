package engine.utils;

import io.qameta.allure.Allure;
import io.restassured.response.Response;

import java.util.concurrent.TimeUnit;

public final class ReportUtil {
    private ReportUtil() {
    }

    public static Response logResponseTime(Response response) {
        Allure.addAttachment("Response time",
                Double.toString(response.getTimeIn(TimeUnit.MILLISECONDS) / 1000.0));
        return response;
    }
}