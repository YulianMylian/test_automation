package engine.utils;

import domain.models.salesforce.SalesforceAccessTokenModel;
import domain.models.salesforce.SalesforceSession;
import domain.models.Credentials;
import engine.api.QueryParameters;
import io.restassured.http.ContentType;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public final class SalesforceLoginUtil {
    private static final String LOGIN_ENDPOINT = "/services/oauth2/token";
    private static final Map<Credentials, SalesforceSession> ACCESS_TOKENS = new HashMap<>();

    private static SalesforceSession login(final Credentials credentials) {
        final var response = given().when().baseUri(credentials.getSalesForceLoginUrl()).contentType(ContentType.JSON)
                .queryParam(QueryParameters.GRANT_TYPE.getQueryParameter(), "password")
                .queryParam(QueryParameters.CLIENT_ID.getQueryParameter(),  credentials.getSalesForceClientId())
                .queryParam(QueryParameters.CLIENT_SECRET.getQueryParameter(), credentials.getSalesForceClientSecret())
                .queryParam(QueryParameters.REDIRECT_URL.getQueryParameter(), credentials.getSalesForceRedirectUrl())
                .queryParam(QueryParameters.USERNAME.getQueryParameter(), credentials.getUsername())
                .queryParam(QueryParameters.PASSWORD.getQueryParameter(), credentials.getPassword())
                .post(LOGIN_ENDPOINT);

        SalesforceAccessTokenModel salesforceAccessTokenModel = response.as(SalesforceAccessTokenModel.class);

        return new SalesforceSession(salesforceAccessTokenModel.getAccessToken(), salesforceAccessTokenModel.getInstanceUrl(), LocalDateTime.now());
    }

    public static SalesforceSession getSession(final Credentials credentials) {
        if (ACCESS_TOKENS.containsKey(credentials)) {
            final var session = ACCESS_TOKENS.get(credentials);
            int refreshSessionTime = 30;
            if (session.getCreationTime().isBefore(LocalDateTime.now().minusMinutes(refreshSessionTime))) {
                ACCESS_TOKENS.put(credentials, login(credentials));
            }
            return ACCESS_TOKENS.get(credentials);
        } else {
            ACCESS_TOKENS.put(credentials, login(credentials));
            return ACCESS_TOKENS.get(credentials);
        }
    }
}