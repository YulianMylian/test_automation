package engine.utils;

import domain.models.Credentials;
import domain.models.heroku.HerokuAccessTokenModel;
import domain.models.heroku.HerokuSession;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class HerokuLoginUtil {
    private static final String LOGIN_ENDPOINT = "/oauth/authorizations";
    private static final Map<Credentials, HerokuSession> ACCESS_TOKENS = new HashMap<>();
    public static final String HEROKU_HEADER_MEDIA_TYPE = "application/vnd.heroku+json; version=3";

    private static HerokuSession login(final Credentials credentials) {
        final var response = given().when().baseUri(credentials.getHerokuBaseUrl()).contentType(JSON)
                .header("Authorization", "Bearer " + credentials.getHerokuApiKey())
                .accept(HEROKU_HEADER_MEDIA_TYPE)
                .post(LOGIN_ENDPOINT);

        HerokuAccessTokenModel herokuAccessTokenModel = response.as(HerokuAccessTokenModel.class);

        return new HerokuSession(herokuAccessTokenModel.getAccessToken().getToken(), LocalDateTime.now());
    }

    public static HerokuSession getSession(final Credentials credentials) {
        if (ACCESS_TOKENS.containsKey(credentials)) {
            final var session = ACCESS_TOKENS.get(credentials);
            int refreshSessionTime = 30;
            if (session.getCreationTime().isBefore(LocalDateTime.now().minusMinutes(refreshSessionTime))) {
                ACCESS_TOKENS.put(credentials, login(credentials));
            }
            return ACCESS_TOKENS.get(credentials);
        } else {
            ACCESS_TOKENS.put(credentials, login(credentials));
            return ACCESS_TOKENS.get(credentials);
        }
    }
}