package engine.utils;

import domain.models.Credentials;
import engine.api.Services;
import io.restassured.http.Header;

public final class TokenProvider {

    private TokenProvider() {

    }

    public static Header getAuthorizationHeader(Credentials credentials, Services service) {
        Header header = null;
        switch (service) {
            case SALESFORCE:
                header = new Header("Authorization", "OAuth " + getSalesForceToken(credentials));
                break;
            case HEROKU:
                header = new Header("Authorization", "Bearer " + getHerokuToken(credentials));
                break;
        }
        return header;
    }

    private static String getSalesForceToken(Credentials credentials) {
        return SalesforceLoginUtil.getSession(credentials).getAccessToken();
    }

    private static String getHerokuToken(Credentials credentials) {
        return HerokuLoginUtil.getSession(credentials).getAccessToken();
    }
}