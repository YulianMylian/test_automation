package ssh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import lombok.Data;
import lombok.extern.java.Log;
import org.assertj.core.api.Fail;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * This class provides a SSH connection functionality.
 */
@Log
@Data
public class SSHConnection {
    public static final int DELAY_BEFORE_READING_OUTPUT = 3000;
    public static final int DELAY_BEFORE_READING_NEXT_BYTE_OF_STREAM = 1000;
    private String hostName;
    private String userName;
    private String password;

    /**
     * Create ssh-session.
     *
     */
    public Session connectToSession() {
        Session session = null;
        try {
            session = new JSch().getSession(userName, hostName, 22);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
        } catch (JSchException e) {
            Fail.fail(e.getMessage());
        }
        return session;
    }

    /**
     * Execute ssh command.
     *
     * @param command {@link String} ssh command to execute.
     */
    public String executeSSHCommandAndGetOutput(String command) {
        log.info("Execute ssh command: " + command);
        Session session = connectToSession();
        ChannelExec channel = null;
        StringBuilder output = null;

        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            channel.setExtOutputStream(outputStream, true);
            channel.setErrStream(outputStream, true);
            channel.setOutputStream(outputStream, true);
            InputStream in = channel.getExtInputStream();
            channel.connect();
            try {
                Thread.sleep(DELAY_BEFORE_READING_OUTPUT);
            } catch (InterruptedException e) {
                Fail.fail(e.getMessage());
            }
            output = readInputStream(channel, new ByteArrayInputStream(outputStream.toByteArray())).append("\n")
                    .append(readInputStream(channel, in));
            log.info("Result: " + output);
            in.close();
        } catch (JSchException | IOException e) {
            Fail.fail(e.getMessage());
        }
        channel.disconnect();
        session.disconnect();
        return output.toString();
    }

    private StringBuilder readInputStream(ChannelExec channel, InputStream in) {
        StringBuilder output = new StringBuilder();
        byte[] array = new byte[1024];

        try {
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(array, 0, 1024);
                    output.append(new String(array, 0, i));
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) {
                        continue;
                    }
                    break;
                }
                try {
                    Thread.sleep(DELAY_BEFORE_READING_NEXT_BYTE_OF_STREAM);
                } catch (InterruptedException e) {
                    Fail.fail(e.getMessage());
                }
            }
        } catch (IOException e) {
            Fail.fail(e.getMessage());
        }
        return output;
    }
}