package utils;

import java.util.Objects;

public final class FilesUtil {

    private FilesUtil() {
    }

    public static String getResourceFilePath(String resourceName) {
        var resource = FilesUtil.class
                .getClassLoader()
                .getResource(resourceName);

        return Objects.isNull(resource) ? "" : resource.getPath();
    }
}
