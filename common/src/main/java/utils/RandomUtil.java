package utils;

import java.util.*;
import java.util.stream.Collectors;

public final class RandomUtil {
    private RandomUtil() {
    }

    public static <T> T getRandomListElement(List<T> list) {
        if (list.isEmpty()) {
            throw new IllegalStateException("Impossible to get random element from empty list");
        }

        int elementIndex = new Random().nextInt(list.size());
        return list.get(elementIndex);
    }

    public static <K, V> Map.Entry<K, V> getRandomMapEntry(Map<K, V> map) {
        if (map.isEmpty()) {
            throw new IllegalStateException("Impossible to get random element from empty map");
        }

        List<Map.Entry<K, V>> entries = new ArrayList<>(map.entrySet());
        return getRandomListElement(entries);
    }

    public static <T extends Enum<?>> T getRandomEnum(Class<T> enumeration) {
        return RandomUtil.getRandomListElement(Arrays.stream(enumeration
                .getEnumConstants()).collect(Collectors.toList()));
    }

    public static <T extends Enum<?>> List<T> getDifferentRandomEnums(Class<T> enumeration, int amount) {
        List<T> allEnumConstants = Arrays.stream(enumeration.getEnumConstants())
                .collect(Collectors.toList());
        if (allEnumConstants.size() < amount) {
            throw new IllegalArgumentException(String
                    .format("Amount [%s] can not be bigger than total amount of enums [%s]",
                            amount, allEnumConstants.size()));
        }

        List<T> selectedEnumConstants = Arrays.stream(enumeration.getEnumConstants())
                .collect(Collectors.toList());
        selectedEnumConstants.clear();
        T randomEnum;
        for (int i = 0; i < amount; i++) {
            randomEnum = RandomUtil.getRandomListElement(allEnumConstants);
            selectedEnumConstants.add(randomEnum);
            allEnumConstants.remove(randomEnum);
        }

        return selectedEnumConstants;
    }

    public static int getRandomIntegerNumberInRange(int min, int max) {
        return new Random().ints(min, (max + 1)).limit(1).findFirst().getAsInt();
    }
}
