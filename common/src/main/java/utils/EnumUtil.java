package utils;

import java.util.function.Predicate;

public final class EnumUtil {

    private EnumUtil() {
    }

    public static <T extends Enum<?>> T searchEnum(Class<T> enumeration,
                                                   Predicate<T> predicate,
                                                   String search) {
        for (T singleEnum : enumeration.getEnumConstants()) {
            if (predicate.test(singleEnum)) {
                return singleEnum;
            }
        }
        throw new IllegalArgumentException(String.format("No enum value was found: Enum[%s] Value[%s]",
                enumeration.getSimpleName(), search));
    }

    public static <T extends Enum<?>> T searchEnum(Class<T> enumeration,
                                                   String search) {
        return searchEnum(enumeration, s -> s.name().equalsIgnoreCase(search), search);
    }

    public static <T extends Enum<?>> T searchEnumByToString(Class<T> enumeration,
                                                             String search) {
        return searchEnum(enumeration, s -> s.toString().equalsIgnoreCase(search), search);
    }

    public static <T extends Enum<?>> T searchEnumByToStringIgnoringSpaces(Class<T> enumeration,
                                                                           String search) {
        String finalSearch = StringUtil.removeSpaces(search);
        return searchEnum(enumeration, s -> StringUtil.removeSpaces(s.toString()).equalsIgnoreCase(finalSearch), search);
    }

    public static <T extends Enum<?>> T getFirstEnum(Class<T> enumeration) {
        T[] enumConstants = enumeration.getEnumConstants();
        if (enumConstants.length == 0) {
            throw new IllegalArgumentException(String.format("Enum[%s] is empty", enumeration.getSimpleName()));
        }

        return enumConstants[0];
    }
}