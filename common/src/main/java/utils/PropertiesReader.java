package utils;

import exceptions.PropertyReaderException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public final class PropertiesReader {

    private static final String CONFIG_PROPERTIES = "config.properties";
    private static Properties PROPERTIES;

    private PropertiesReader() {
    }

    public static String getProperty(String propertyName) {
        String systemPropertyValue = System.getProperty(propertyName);
        if (Objects.nonNull(systemPropertyValue)) {
            return systemPropertyValue;
        }

        if (Objects.isNull(PROPERTIES)) {
            synchronized (PropertiesReader.class) {
                if (Objects.isNull(PROPERTIES)) {
                    try (InputStream reader = PropertiesReader.class
                            .getClassLoader().getResourceAsStream(CONFIG_PROPERTIES)) {

                        Properties properties = new Properties();
                        properties.load(reader);
                        PROPERTIES = properties;
                    } catch (IOException ex) {
                        throw new PropertyReaderException("Error occurred during reading properties file: "
                                + ex.getMessage());
                    }
                }
            }
        }

        return PROPERTIES.getProperty(propertyName, "");
    }

    public static Boolean getBooleanProperty(String propertyName) {
        return Boolean.valueOf(getProperty(propertyName));
    }
}