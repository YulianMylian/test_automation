package utils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public final class CollectionsUtil {

    private CollectionsUtil() {
    }

    public static List<String> filterOutEmptyStrings(List<String> strings) {
        return strings.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> collection, Function<T, K> groupBy) {
        return collection.stream().collect(groupingBy(groupBy, Collectors.toList()));
    }

    public static <T, K> Map<K, List<T>> filterMapEntriesHavingAmount(Map<K, List<T>> map, int amount) {
        return map.entrySet().stream().filter(e -> e.getValue().size() == amount)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
