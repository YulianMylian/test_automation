package utils;

import com.github.javafaker.Faker;
import lombok.extern.java.Log;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Log
public final class StringUtil {

    private static final String SEARCH_TERM_SPLIT_REGEX = " ";
    private static final int SEARCH_TERM_MINIMUM_LENGTH = 3;
    private static final int SEARCH_TERM_MINIMUM_REPEATS = 5;

    private StringUtil() {
    }

    public static String getEmptyStringIfNull(String string) {
        return Objects.isNull(string) ? "" : string;
    }

    public static int getZeroIfStringIsEmpty(String string) {
        return string.isEmpty() ? 0 : Integer.parseInt(string);
    }

    public static double getMoneyValue(String string) {
        if (string.isEmpty()) {
            return 0;
        }

        string = removeSpaces(string);
        return Double.parseDouble(string.replaceAll("\\$", "").replaceAll(",", ""));
    }

    public static double getDouble(String string) {
        if (string.isEmpty()) {
            return 0;
        }

        return Double.parseDouble(string);
    }

    public static String removeSpaces(String string) {
        return string.replaceAll(" ", "");
    }

    public static Map<String, Long> getCommonSearchTermsCounts(List<String> strings) {
        return strings.stream()
                .map(s -> s.split(SEARCH_TERM_SPLIT_REGEX))
                .flatMap(Arrays::stream)
                .filter(s -> s.length() > SEARCH_TERM_MINIMUM_LENGTH)
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > SEARCH_TERM_MINIMUM_REPEATS)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static String getSearchTermFromString(String string) {
        String[] splits = string.split(" ");
        if (splits.length == 1) {
            return string;
        }
        String searchTerm = splits[0];
        for (String split : splits) {
            if (split.length() > searchTerm.length()) {
                searchTerm = split;
            }
        }
        log.warning(String.format("Using [%s] from [%s] as search term to overcome full search text absence",
                searchTerm, string));
        return searchTerm;
    }

    public static String getFirstWordFromString(String string) {
        String[] split = string.split(" ");
        if (split.length == 1) {
            return string;
        }

        return split[0];
    }

    public static boolean containsIgnoreCase(String outerValue, String innerValue) {
        return outerValue.toLowerCase().contains(innerValue.toLowerCase());
    }

    public static String removeExtraSpacesBetweenStrings(String value) {
        return value.replaceAll(" +", " ");
    }

    public static String formatStringToLocalDateIfDate(String value, DateTimeFormatter formatter) {
        try {
            return LocalDate.parse(value, formatter).toString();
        } catch (DateTimeParseException e) {
            return value;
        }
    }

    public static String buildPhoneNumberInUIFormat() {
        var faker = new Faker();
        return String.format("(%s) %s-%s", faker.number().digits(3), faker.number().digits(3), faker.number().digits(4));
    }

    public static String buildSsnInUIFormat() {
        var faker = new Faker();
        return String.format("%s-%s-%s", faker.number().digits(3), faker.number().digits(2), faker.number().digits(4));
    }

    public static String formatOpportunityAmountDueOptions(String opportunityAmountDueOption) {
        return removeExtraSpacesBetweenStrings(opportunityAmountDueOption.replaceAll("\\$", "").replaceAll("\n+", "").trim());
    }
}