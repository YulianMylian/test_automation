package utils;

import java.time.*;
import java.time.temporal.ChronoField;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

public final class DateTimeUtil {
    private DateTimeUtil() {

    }

    public static LocalTime roundTimeToClosestInTimePicker(LocalTime time) {
        List<Integer> timePickerMinutesGradation = List.of(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55);

        int providedMinute = time.get(ChronoField.MINUTE_OF_HOUR);
        int closestPickerValue = timePickerMinutesGradation.stream()
                .min(Comparator.comparingInt(i -> Math.abs(i - providedMinute)))
                .orElseThrow(() -> new NoSuchElementException(String
                        .format("Can not select closest to [%s] from %s", providedMinute, timePickerMinutesGradation)));

        return time.withMinute(closestPickerValue).withSecond(0).withNano(0);
    }

    public static LocalDateTime getTimeInCurrentTimeZone(String dateTime) {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTime);
        return zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static int getMinutesDiff(LocalDateTime earlierDate, LocalDateTime laterDate) {
        Duration duration = Duration.between(earlierDate, laterDate);
        return (int) Math.abs(duration.toMinutes());
    }
}
