package mail;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public final class MailUtil {
    private MailConnectivityModel connectEntity;

    public MailUtil(EmailAccountModel account) {
        this.connectEntity = MailConnectivityBuilder.build(account);
    }

    public void send(List<String> recipients, String subject, String text, String mimeText, Path attachment) {
        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(connectEntity.getEmailAccount().getUsername(), connectEntity.getEmailAccount().getPassword());
            }
        };
        Session session = Session.getInstance(connectEntity.getSendingProperties(), authenticator);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(connectEntity.getEmailAccount().getUsername()));
            message.setRecipients(Message.RecipientType.TO, recipients.stream().map(address -> {
                try {
                    return new InternetAddress(address);
                } catch (AddressException e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList()).toArray(new Address[recipients.size()]));
            message.setSubject(subject);
            message.setText(text);
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(mimeText);
            MimeBodyPart mbp2 = new MimeBodyPart();
            mbp2.attachFile(attachment.toFile());
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);
            message.setContent(mp);
            Transport.send(message);
        } catch (MessagingException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
