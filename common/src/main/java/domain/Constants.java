package domain;

import java.time.format.DateTimeFormatter;

/**
 * Common constants.
 */
public interface Constants {
    DateTimeFormatter FIRST_DATE_FORMAT = DateTimeFormatter.ofPattern("M/d/yy");
    DateTimeFormatter SECOND_DATE_FORMAT = DateTimeFormatter.ofPattern("M/d/yyyy");
    DateTimeFormatter THIRD_DATE_FORMAT = DateTimeFormatter.ofPattern("MMM d, yyyy");
    DateTimeFormatter FOURTH_DATE_FORMATTER = DateTimeFormatter.ofPattern("MMM d, yyyy, h:mm:ss a");
}