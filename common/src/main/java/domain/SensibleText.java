package domain;

import lombok.Data;

import java.util.Objects;

/**
 * Helps to hide sensible data from Allure report.
 */
@Data
public class SensibleText {
    private final String text;

    @Override
    public String toString() {
        if (Objects.isNull(text)) {
            return "";
        }

        return "*".repeat(text.length());
    }
}
