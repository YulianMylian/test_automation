package domain.models.heroku;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Heroku authentication access token response model.
 */
@Data
public class HerokuAccessTokenModel {
    @JsonProperty("access_token")
    private HerokuAccessToken accessToken;
    @JsonProperty("client")
    private String client;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("description")
    private String description;
    @JsonProperty("grant")
    private String grant;
    @JsonProperty("id")
    private String id;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("session")
    private String session;
    @JsonProperty("scope")
    private List<String> scope;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("user")
    private HerokuUser user;
}
