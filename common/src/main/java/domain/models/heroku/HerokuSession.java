package domain.models.heroku;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Heroku session which include access token,
 * creation time and the token expiring time values.
 */
@Data
public class HerokuSession {
    public HerokuSession(final String accessToken, final LocalDateTime creationTime) {
        this.accessToken = accessToken;
        this.creationTime = creationTime;
    }

    private String accessToken;

    private LocalDateTime creationTime;
}