package domain.models.heroku;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Access token object of the Heroku authentication access token response model.
 */
@Data
public class HerokuAccessToken {
    @JsonProperty("expires_in")
    private String expiresIn;
    @JsonProperty("id")
    private String id;
    @JsonProperty("token")
    private String token;
}
