package domain.models.heroku;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * User object of the Heroku authentication access token response model.
 */
@Data
public class HerokuUser {
    @JsonProperty("id")
    private String id;
    @JsonProperty("email")
    private String email;
    @JsonProperty("full_name")
    private String fullName;
}