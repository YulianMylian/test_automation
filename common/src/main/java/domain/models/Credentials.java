package domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import utils.PropertiesReader;

/**
 * Getting services credentials {@link engine.api.Services}
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Credentials {

    private String username;
    private String password;

    private String salesForceLoginUrl;
    private String salesForceClientId;
    private String salesForceClientSecret;
    private String salesForceRedirectUrl;

    private String herokuBaseUrl;
    private String herokuApiKey;

    public static Credentials getSynapseRtCredential() {
        String username = PropertiesReader.getProperty("synapsert.user.username");
        String password = PropertiesReader.getProperty("synapsert.user.password");

        return Credentials.builder().username(username).password(password).build();
    }

    public static Credentials getSalesForceCredential() {
        String username = PropertiesReader.getProperty("salesforce.user.username");
        String password = PropertiesReader.getProperty("salesforce.user.password");
        String salesForceLoginUrl = PropertiesReader.getProperty("salesforce.login.url");
        String salesForceClientId = PropertiesReader.getProperty("salesforce.client.id");
        String salesForceClientSecret = PropertiesReader.getProperty("salesforce.client.secret");
        String salesForceRedirectUrl = PropertiesReader.getProperty("salesforce.redirect.url");

        return Credentials.builder().username(username).password(password)
                .salesForceLoginUrl(salesForceLoginUrl).salesForceClientId(salesForceClientId)
                .salesForceClientSecret(salesForceClientSecret).salesForceRedirectUrl(salesForceRedirectUrl).build();
    }

    public static Credentials getHerokuCredential() {
        String username = PropertiesReader.getProperty("heroku.user.username");
        String password = PropertiesReader.getProperty("heroku.user.password");
        String herokuBaseUrl = PropertiesReader.getProperty("heroku.base.url");
        String herokuApiKey = PropertiesReader.getProperty("heroku.user.apikey");

        return Credentials.builder().username(username).password(password).herokuBaseUrl(herokuBaseUrl)
                .herokuApiKey(herokuApiKey).build();
    }
}