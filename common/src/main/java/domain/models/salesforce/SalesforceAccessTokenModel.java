package domain.models.salesforce;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * SalesForce authentication access token response model.
 */
@Data
public class SalesforceAccessTokenModel {
    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("instance_url")
    private String instanceUrl;
    private String id;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("issued_at")
    private String issuedAt;
    private String signature;
}