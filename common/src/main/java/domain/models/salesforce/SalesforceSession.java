package domain.models.salesforce;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * SalesForce session which include access token,
 * instance url and creation time values.
 */
@Data
public class SalesforceSession {
    public SalesforceSession(final String accessToken, final String instanceUrl, final LocalDateTime creationTime) {
        this.accessToken = accessToken;
        this.instanceUrl = instanceUrl;
        this.creationTime = creationTime;
    }

    private String accessToken;

    private LocalDateTime creationTime;

    private String instanceUrl;
}