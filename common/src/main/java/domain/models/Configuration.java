package domain.models;

import engine.api.authenticationstrategy.AuthenticationStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class provides a basic configuration for submitting API requests.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Configuration {
    private String baseUrl;
    private AuthenticationStrategy authenticationStrategy;
}